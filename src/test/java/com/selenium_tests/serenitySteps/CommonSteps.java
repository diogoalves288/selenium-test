package com.selenium_tests.serenitySteps;


import com.selenium_tests.ui.BasePage;
import lombok.extern.java.Log;


@Log
public class CommonSteps {

    BasePage basePage = new BasePage();


    public void goToDefaultUrl () {

        log.info("Going to default web page");
        basePage.goToDefaultPage();

    }


    public void waitForPageToLoad () {

        basePage.waitToLoad();
    }


    public void searchForWord (String textToSearch) {

        basePage.writeToSearch(textToSearch);
        basePage.clickToSearch();
    }


    public void waitForContentPageToLoad () {

        basePage.waitForContentPageToLoad();
    }


    public String returnContentPageHeader () {

        String header = basePage.getContentPageHeader();
        log.info("The word in the header is: " + header);
        return header;
    }


    public String returnFooterInfo () {

        String footerInfo = basePage.getFooterInfo();
        log.info("The footer info is: " + footerInfo);
        return footerInfo;
    }


}
