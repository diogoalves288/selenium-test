package com.selenium_tests.cucumberDefinition;


import com.selenium_tests.serenitySteps.CommonSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import static org.hamcrest.Matchers.containsString;


public class CommonCucumber {

    @Steps
    CommonSteps commonSteps;


    @Given("^the user opens the website$")
    public void theUserOpensTheWebsite () {

        commonSteps.goToDefaultUrl();
        commonSteps.waitForPageToLoad();
    }


    @When("^the user search for word \"([^\"]*)\" in the search wrap$")
    public void theUserSearchForWordInTheSearchWrap (String searchWord) {

        commonSteps.searchForWord(searchWord);
    }


    @Then("^the content page opens with search word \"([^\"]*)\"$")
    public void theContentPageOpensWithSearchWord (String word) {

        commonSteps.waitForContentPageToLoad();
        Assert.assertThat(
            commonSteps.returnContentPageHeader(),
            containsString(word)
        );

    }


    @Then("^the footer info contains the last edit date \"([^\"]*)\"$")
    public void theFooterInfoContainsTheLastEditDate (String lastmod) {

        Assert.assertThat(
            commonSteps.returnFooterInfo(),
            containsString(lastmod)
        );

    }


}
