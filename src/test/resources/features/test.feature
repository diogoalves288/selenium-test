Feature: Selenium Test Automation Challenge


  Scenario Outline: Validate the definitions of some words
    Given the user opens the website
    When the user search for word "<word>" in the search wrap
    Then the content page opens with search word "<word>"
    And the footer info contains the last edit date "<lastmod>"
#    The next step was skipped because of DOM complexity
#    And one of the definitions present in the page is "<definition>"


    Examples:
      | word  | lastmod                                                 | definition                                                                                     |
      | apple | This page was last edited on 26 March 2019, at 11:22.   | A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates.  |
      | pear  | This page was last edited on 5 February 2019, at 23:51. | An edible fruit produced by the pear tree, similar to an apple but elongated towards the stem. |


#  Possible solution to implement the last step
#
#  Have 2 lists, the first with the entries of the words of the sentence to validate. (separated by space)
#  The second list will have the words obtained from the text of the elements for the <li> and the elements below the <li> that are <a>
#  Then it is to compare that all words from the first list exist in the second list
#  And that the size of the lists is the same