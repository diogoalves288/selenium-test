package com.selenium_tests.ui.actions;


import com.selenium_tests.model.browser.BrowserFactory;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;


@Slf4j
public class SeleniumActions implements WebPageActions {

    protected final WebDriver driver;

    protected final WebDriverWait wait;


    public SeleniumActions () {


        this.driver = BrowserFactory.getDriverForBrowser();
        this.wait = new WebDriverWait(driver, 30);
    }


    @Override
    public void navigateToUrl (String url) {

        driver.navigate().to(url);
        log.debug("Redirect browser to url: ()", url);
    }


    @Override
    public WebElement findElement (By by) {

        log.debug("Trying to find element with locator: " + by);
        return this.driver.findElement(by);
    }


    @Override
    public boolean isElementPresent (By by) {

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }


    @Override
    public boolean isElementVisible (WebElement element) {

        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }


    @Override
    public String getTextOfElement (WebElement element) {

        final String text = element.getText();
        log.debug("Retrieve text '{}' from element: {}", text, element);
        return text;
    }


    @Override
    public void cleanOnElement (WebElement element) {

        this.selectAllTextInElement(element);
        element.sendKeys(Keys.DELETE);
        log.debug("Delected all text inside element: {}", element);
    }


    @Override
    public void clickOnElement (WebElement element) {

        element.click();
        log.debug("Clicked on element: {}", element);

    }


    @Override
    public void writeOnElement (WebElement element, String text) {

        this.cleanOnElement(element);
        element.sendKeys(text);
        log.debug("Write '{}' inside element: {}", text, element);
    }


    @Override
    public void setValueOfCheckBox (WebElement element, boolean isChecked) {

    }


    @Override
    public void setOptionOfSelectByValue (WebElement element, String optionValue) {

    }


    @Override
    public void setOptionOfSelectByIndex (WebElement element, int index) {

    }


    @Override
    public void setOptionOfSelectByText (WebElement element, String text) {

    }


    @Override
    public void selectAllTextInElement (WebElement element) {

        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        log.debug("Selected all text inside element: {}", element);
    }


    @Override
    public <V> V waitUntil (Function<? super WebDriver, V> condition) {

        log.debug("Waiting for condition: " + condition.toString());
        return wait.until(condition);
    }


}
