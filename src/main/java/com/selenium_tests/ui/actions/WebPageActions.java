package com.selenium_tests.ui.actions;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.function.Function;


public interface WebPageActions {


    void navigateToUrl (String url);


    WebElement findElement (By by);


    boolean isElementPresent (By by);


    boolean isElementVisible (WebElement element);


    String getTextOfElement (WebElement element);


    void cleanOnElement (WebElement element);


    void clickOnElement (WebElement element);


    void writeOnElement (WebElement element, String text);


    void setValueOfCheckBox (WebElement element, boolean isChecked);


    void setOptionOfSelectByValue (WebElement element, String optionValue);


    void setOptionOfSelectByIndex (WebElement element, int index);


    void setOptionOfSelectByText (WebElement element, String text);


    void selectAllTextInElement (WebElement element);


    default boolean isElementVisibleWithLocator (By by) {

        return this.isElementVisible(this.findElement(by));
    }


    default String getTextOfElementWithLocator (By by) {

        return this.getTextOfElement(this.findElement(by));
    }


    default void cleanOnElementWithLocator (By by) {

        this.cleanOnElement(this.findElement(by));

    }


    default void clickOnElementWithLocator (By by) {

        this.clickOnElement(this.findElement(by));

    }


    default void writeOnElementWithLocator (By by, String text) {

        this.writeOnElement(this.findElement(by), text);

    }


    default void selectAllTextInElementWithLocator (By by) {

        this.selectAllTextInElement(this.findElement(by));

    }


    <V> V waitUntil (Function<? super WebDriver, V> condition);
}
