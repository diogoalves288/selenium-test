package com.selenium_tests.ui;


import com.selenium_tests.Configurations;
import com.selenium_tests.ui.actions.SeleniumActions;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


@Slf4j
public class BasePage extends SeleniumActions {


    @FindBy(how = How.XPATH, using = "//div[@class='bodySearchWrap']//*[@class='mw-ui-input mw-ui-input-inline']")
    WebElement searchWrap;

    @FindBy(how = How.XPATH, using = "//div[@class='bodySearchWrap']//*[@class='mw-ui-button']")
    WebElement searchButton;

    @FindBy(how = How.ID, using = "ca-nstab-main")
    WebElement entryTab;

    @FindBy(how = How.ID, using = "firstHeading")
    WebElement contentPageHeader;

    @FindBy(how = How.ID, using = "footer-info-lastmod")
    WebElement footerInfo;


    public BasePage () {

        super();
        PageFactory.initElements(driver, this);
    }


    /**
     * Method to go to default page
     */
    public void goToDefaultPage () {

        navigateToUrl(Configurations.configs.getSelenium().getDefaultUrl());
    }


    /**
     * Method to wait for default page to load
     */
    public void waitToLoad () {

        waitUntil(ExpectedConditions.visibilityOf(searchWrap));
    }


    /**
     * Method to write in the search box
     *
     * @param textToSearch text that is written on the search box
     */
    public void writeToSearch (String textToSearch) {

        writeOnElement(searchWrap, textToSearch);
    }


    /**
     * Method to click on the search button
     */
    public void clickToSearch () {

        clickOnElement(searchButton);
    }


    /**
     * Method to wait for content page to load
     */
    public void waitForContentPageToLoad () {

        waitUntil(ExpectedConditions.visibilityOf(entryTab));
    }


    /**
     * Method to get the text of the header
     *
     * @return returns the text of the page header
     */
    public String getContentPageHeader () {

        return getTextOfElement(contentPageHeader);
    }


    /**
     * Method that gets the text of the footer info
     *
     * @return returns the last edit date
     */
    public String getFooterInfo () {

        return getTextOfElement(footerInfo);
    }

}