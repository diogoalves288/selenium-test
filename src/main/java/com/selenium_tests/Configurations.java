package com.selenium_tests;


import com.selenium_tests.model.config.YamlConfig;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;


@Slf4j
public class Configurations {

    public static final YamlConfig configs;

    static {
        try (
            InputStream inputStream = Thread
                .currentThread()
                .getContextClassLoader()
                .getResourceAsStream("configuration-qa.yaml")
        ) {

            configs = new Yaml().loadAs(inputStream, YamlConfig.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    private Configurations () {}
}




