package com.selenium_tests.model.config;


import lombok.Data;


@Data
public class Selenium {

    private String defaultUrl;

    private String browser;

    private double version;

}
