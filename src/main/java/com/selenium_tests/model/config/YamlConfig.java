package com.selenium_tests.model.config;


import lombok.Data;


@Data
public class YamlConfig {

    private Selenium selenium;
}
