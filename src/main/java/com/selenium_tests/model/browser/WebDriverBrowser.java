package com.selenium_tests.model.browser;


import lombok.NonNull;


public class WebDriverBrowser extends SelenoidBrowser {

    @NonNull
    private String browserConfig;


    public WebDriverBrowser (@NonNull String browserType, String browserConfig) {

        super(browserType);
        this.browserConfig = browserConfig;

    }


    @Override
    public void startDockerServer () {
        //TODO start docker specific for WebDriver Location
    }

}
