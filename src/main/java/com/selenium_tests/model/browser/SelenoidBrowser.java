package com.selenium_tests.model.browser;


import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;


@RequiredArgsConstructor
public class SelenoidBrowser {

    @NonNull
    private String browserType;


    Capabilities getCapabilities (double version) {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browserType);
        capabilities.setVersion(Double.toString(version));
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);

        return capabilities;
    }

    public void startDockerServer () {
        //TODO start docker container for standard selenoid images
    }

    public void stopDockerServer () {
        //TODO stop docker container for standard selenoid images
    }
}
