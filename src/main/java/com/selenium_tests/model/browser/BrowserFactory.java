package com.selenium_tests.model.browser;


import com.selenium_tests.Configurations;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;


@Slf4j
public class BrowserFactory {

    private BrowserFactory () {}


    public static WebDriver getDriverForBrowser () {

        SelenoidBrowser selenoidBrowser = getBrowserFromConfiguration();

        try {
            return new RemoteWebDriver(
                URI.create("http://localhost:4444/wd/hub").toURL(),
                selenoidBrowser.getCapabilities(Configurations.configs.getSelenium().getVersion())
            );
        } catch (MalformedURLException e) {
            log.error("Error creating URL to Selenoid", e);
            throw new ExceptionInInitializerError(e);
        }
    }


    public static SelenoidBrowser getBrowserFromConfiguration () {

        SelenoidBrowser browser;

        String browserName = Configurations.configs.getSelenium().getBrowser();
        switch (browserName) {
            case BrowserType.FIREFOX:
            case BrowserType.CHROME:
            case BrowserType.SAFARI:
            case BrowserType.PHANTOMJS:
                browser = new SelenoidBrowser(browserName);
                break;

            case BrowserType.EDGE:
                browser = new IExplorerBrowser();
                break;

            default:
                throw new IllegalArgumentException(
                    "Browser configured not recognized, value was :" + browserName
                );

        }

        return browser;
    }
}
