package com.selenium_tests.model.browser;


import org.openqa.selenium.remote.BrowserType;


public class EdgeBrowser extends WebDriverBrowser{

    public EdgeBrowser () {

        super(BrowserType.EDGE, "edgeBrowser");
    }


}
