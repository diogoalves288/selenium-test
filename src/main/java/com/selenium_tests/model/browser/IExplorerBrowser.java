package com.selenium_tests.model.browser;


import org.openqa.selenium.remote.BrowserType;


public class IExplorerBrowser extends WebDriverBrowser{

    public IExplorerBrowser () {

        super(BrowserType.IE, "ieBrowser");
    }


}