# Serenity BDD with Selenium and Selenoid
Originally cloned from Serenity BDD - FE Web seed project that is intended to fast track the creation and configuration of a **TAS** (Test Automation Solution) for **Front End Scenarios**.<br/>
This is a simple project to demo how to use Serenity BDD, Selenium and Selenoid to test UI.
Selenoid is a successful alternative to SeleniumGrid and docker-selenium. It provides a highly configurable execution environment. More importantly, it creates a fresh browser session for each test.
It uses Gradle and Java 8.

## Getting Started

### Prerequisites

- Any software to clone this repository (e.g. SourceTree, Git installed locally...)
- Java 8 JDK
- Docker Desktop
- Selenium
- Selenoid

**NOTE:** Gradle local installation is not needed because is integrated in the project with the Wrapper.

### Installing

Clone this repository

## Running the tests

1.Start Docker Desktop

2.Start Selenoid:

a. Open a PowerShell window in the Selenoid installation folder and execute the following commands

```
.\cm_windows_amd64.exe selenoid start --vnc
```
```	
.\selenoid.exe
```

b. Open a new PowerShell window in the Selenoid installation folder and execute the following command
```
.\selenoid-ui.exe
```

3.Use below command to execute the tests:

```
gradlew clean test aggregate
```

4.Tests running in the UI can be viewed on the localhost

```
http://localhost:8080/#/
```

5.If all works fine you will find an index.html file inside **target/site/serenity** folder with the results of the tests.


#### Scope
 - Serenity BDD (Cucumber based) framework schema
 - Serenity BDD, Cucumber and Selenium to test UI
 - Execution WebPageActions: navigateToUrl, findElement, isElementVisible, getTextOfElement, clickOnElement, etc.
 - Validation of the content of the elements (text)
 - Execute features


### Troubleshooting

- "On IntelliJ I can't go to the Step definitions of any step in a feature file, it doesn't detect the definition and suggests me to create one"
> Answer: Make sure you have the `Cucumber for Java` plugin installed and deactivate the `Substeps IntelliJ Plugin`. Restart IntelliJ and you should now be able to go to step definition of steps.

- "Running a single scenario from IntelliJ"
> Answer: To run a specific scenario from IntelliJ, for example faced to qa environment, don't forget to add `-Dproperties=serenity_qa.properties` to the `VM Options` to that scenario Run Configuration.

- "The annotations in the project are not working"
> Answer: Install `Lombok Plugin` and go to `File > Setting > Annotation Processors` and enable `Annotation Processing`